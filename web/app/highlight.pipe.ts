import {Pipe, PipeTransform} from "@angular/core";
@Pipe({ name: 'highlight',  pure: false })

export class HighlightPipe implements PipeTransform {
    transform(value: any, args: string = null): any {
        if ("undefined" !== typeof args && "undefined" !== typeof value) {
            let pattern = args.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1").trim();
            return value.replace(new RegExp(pattern, 'i'), "<strong>"+args.trim()+"</strong>");
        } else {
            return value;
        }
    }
}