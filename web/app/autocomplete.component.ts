import {
    Component, OnInit, Input, ElementRef, ViewChild, Injectable, ChangeDetectionStrategy,
    ChangeDetectorRef
} from '@angular/core';
import {WikiService} from "./wiki.service";
import {WikiResult} from "./wiki-result";
import {HighlightPipe} from "./highlight.pipe";
import {CONFIG} from "./config";
@Component({
    selector: 'autocomplete',
    styles: [`
        a{
            text-decoration: none;
            color: black;
        }
        .autocompletePlaceholder{
            position: absolute;
            width: 500px;
            height: 500px;
            top: 0px;
            left: 0px;
            letter-spacing: 1px;
            text-align: left;
        }
        ul, li{
            list-style-type: none;
            padding: 2px;
            margin: 0;
        }
        li.selected{
            background-color: #5eb5e0;
        }
        input[type="text"]{
            width: 500px;
            border: 0;
            border-bottom: 1px solid blue;
            font-size: 30px;
        }
    `],
    template:`
            <input #inputText type="text" placeholder="Sii..." [(ngModel)]="value" (ngModelChange)="onChange($event)" (keydown)="onKeyDown($event)" (keyup)="onKeyUp($event)"/>
            <div #autocompletePlaceholder class="autocompletePlaceholder" [style.display]="items ? 'block' : 'none'">
                <ul *ngIf="items && value">
                    <li *ngFor="let item of items" class="{{true == item.selected ? 'selected' : ''}}"><a target="_blank" href="{{item.url}}" [innerHtml]="item.title | highlight:value"></a></li>
                </ul>
            </div>
          `,
    pipes : [HighlightPipe]
})
export class AutocompleteComponent implements OnInit{

    private value : string = "";
    private selectedId : number = -1;
    private items : WikiResult[];
    private timeout;
    @ViewChild("inputText") inputText;
    @ViewChild("autocompletePlaceholder") autocompletePlaceholder;
    constructor(private _wikiService: WikiService, private ref: ChangeDetectorRef) {}
    
    private adjustDom() {
        this.autocompletePlaceholder.nativeElement.style.top = this.inputText.nativeElement.offsetTop + this.inputText.nativeElement.offsetHeight + "px";
        this.autocompletePlaceholder.nativeElement.style.left = this.inputText.nativeElement.offsetLeft + "px";
        this.autocompletePlaceholder.nativeElement.style.width = this.inputText.nativeElement.offsetWidth + "px";
    }

    private onKeyDown(val) {
        if ("undefined" === typeof this.items || 0 == this.items.length) {
            return true;
        }
        let l = this.items.length;
        if ("undefined" !== typeof this.items[this.selectedId]) {
            this.items[this.selectedId].selected = false;
        }
        if ("ArrowDown" == val.code) {
            if (0 < l && (this.selectedId < l-1 || -1 == this.selectedId)) {
                this.selectedId++;
            } else {
                this.selectedId = 0;
            }
            this.items[this.selectedId].selected = true;
            return false;
        } else if ("ArrowUp" == val.code) {
            if (this.selectedId > 0) {
                this.selectedId--;
            } else {
                this.selectedId = l-1;
            }
            this.items[this.selectedId].selected = true;
            return false;
        } else {
            return true;
        }
    }

    private onKeyUp(val) {
        if ("Enter" == val.code && -1 != this.selectedId) {
            window.location.href = this.items[this.selectedId].url;
        }
    }

    private onChange(val) {
        this.callApi();
    }

    private callApi() {
        clearTimeout(this.timeout);
        if (2 <= this.value.trim().length) {
            this.selectedId = -1;
            this.timeout = setTimeout((val) => {
                this._wikiService.query(this.dataRetrieved, this.value.trim());
            }, CONFIG.typeInterval);
        }
    }

    dataRetrieved = (data : WikiResult[]) => {
        this.items = [];
        this.items = data;
    }

    ngOnInit() {
        this.adjustDom();
    }
}