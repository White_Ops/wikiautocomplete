import {Component, Input} from '@angular/core';
@Component({
    selector: 'wiki-app',
    template:`
          <div class="content">
              <h1>Wiki Autocomplete</h1>
              <autocomplete></autocomplete>
          </div>
          `
})
export class AppComponent{}