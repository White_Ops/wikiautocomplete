import {ApiService} from "./api.service";
import {WikiResult} from "./wiki-result";

export class WikiService extends ApiService {
    public query(callback : {(data : WikiResult[]) : void}, query : string) {
        this.get("?q="+query, callback);
    }
}