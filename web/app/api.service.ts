import 'rxjs/add/operator/toPromise';
import {Http} from "@angular/http";
import {Injectable} from "@angular/core";
import {CONFIG} from "./config";

@Injectable()
export abstract class ApiService{
    private errorCallback : {(error : string) : void} = null;

    constructor(private http: Http) { }

    private handleError(error){
        if (null != this.errorCallback){
            this.errorCallback(error);
        }
    }
    private handleSuccess(callback : {(data : any) : any}, response){
        if (1 == response.status){
            callback(response.data);
        } else {
            this.handleError("API_STATUS_ERROR");
        }
    }
    public setErrorCallback(callback : {(error : string) : void}){
        this.errorCallback = callback;
    }
    public get(path : string, callback : {(data : any) : any}){
        this.http.get(CONFIG.apiBasePath + path)
            .toPromise()
            .then(response => this.handleSuccess(callback,response.json()))
            .catch(error => this.handleError(error));
    }
}