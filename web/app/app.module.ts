import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }  from './app.component';
import {HttpModule} from "@angular/http";
import {AutocompleteComponent} from "./autocomplete.component";
import {WikiService} from "./wiki.service";
import {ApiService} from "./api.service";
import {HighlightPipe} from "./highlight.pipe";
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    declarations: [
        AppComponent,
        AutocompleteComponent,
        HighlightPipe
    ],
    providers: [
        ApiService,
        WikiService
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
