export class WikiResult {
    title : string;
    url : string;
    selected : boolean;
}