<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class WikiController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @Route("/autocomplete")
     */
    public function autocompleteAction(Request $request) {
        $result = ['status' => 0];
        $wikiUtil = $this->get('wiki');
        $wikiUtil->setBaseUrl($this->container->getParameter("api_basePath"));
        $query = trim($request->get("q"));

        if (2 > strlen($query)) {
            return new JsonResponse($result);
        }

        $data = $wikiUtil->query($query);

        if (empty($data)) {
            return new JsonResponse($result);
        }

        $result['status'] = 1;
        $result['data'] = $data;

        return new JsonResponse($result);
    }

}