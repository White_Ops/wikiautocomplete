<?php
namespace AppBundle\Util;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class Wiki
{
    private $baseUrl;

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $query
     * @return array
     */
    public function query($query)
    {
        if (empty($query)) {
            throw new Exception("No query !");
        }

        $cacheKey = $this->getCacheKey($query);
        $cache = new FilesystemAdapter();
        $cacheData = $cache->getItem($cacheKey);

        if ($cacheData->isHit()) {
            return unserialize($cacheData->get());
        } else {
            $fullUrl = $this->getUrl($query);
            $response = $this->callApi($fullUrl);
            $response = json_decode($response, 1);
            if (0 == count($response[1])) {
                return [];
            }
            $data = $this->cleanData($response);
            $cacheData->set(serialize($data));
            $cache->save($cacheData);
            return $data;
        }
    }

    /**
     * Get clear data (from wiki api)
     * @param array $data
     * @return array
     */
    private function cleanData($data)
    {
        $result = [];
        foreach ($data[1] as $index => $item) {
            $result[] = ['title' => $item, 'url' => $data[3][$index]];
        }
        return $result;
    }

    /**
     * Fetch data from wikipedia api
     *
     * @param string $query
     * @return array
     */
    private function callApi($url)
    {
        if (empty($url)) {
            throw new Exception("No url to fetch !");
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    /**
     * @param string $query
     * @return string
     */
    private function getCacheKey($query)
    {
        return md5($this->getUrl($query));
    }

    /**
     * @param string $query
     * @return string
     */
    private function getUrl($query)
    {
        if (empty($this->baseUrl)) {
            throw new Exception("No base url !");
        }
        return str_replace('$Q$', $query, $this->getBaseUrl());
    }
}
