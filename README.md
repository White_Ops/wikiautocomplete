WikiAutocomplete
================

Zadanie napisane w Symfony 3 i Angular 2 (Typescript). Testy jedynie phpunit, z powodu natłoku obowiązków (braku czasu) nie zdążyłem poprawić/napisać paru rzeczy:
+ testów angulara
+ lepiej ostylować
+ enter w autocomplecie nie przechodzi do nowej karty tylko otwiera link w obecnej
+ brak cache po stronie JS (po stronie serwera jest)


Założenia
====

+ Autocomplete zaczyna działać od 2 wpisanych znaków.
+ Czas od ostatniego wpisanego znaku do odwołania do api 300 ms (można zmienić w konfigu apki js'owej)
+ Limit wyników 10 (można zmienić w konfigu)
+ Można "poruszać" się po wynikach za pomocą strzałek (góra i dół) oraz wcisnąć enter na danej pozycji
